package startit.streammapping;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Main {

    public static void main(String[] args) {

        //A list with strings called alpha and create a new ArrayList
        //Add 4 strings(letters) in the list and print it

        List<String> alpha = Arrays.asList("c", "e", "m", "z");

        System.out.println(alpha);

        //Convert the list to stream
        //Use .map to switch strings to upper case
        //Use the terminal operation .collector to transform the elements of the stream into a list

        Object uppercase = alpha.stream()
                .map(u -> u.toUpperCase())
                .collect(toList());

        //Print the new list

        System.out.println(uppercase);

        //Create another list and add 5 integers

        List<Integer> beta = Arrays.asList(3, 5, 7, 9, 6);

        System.out.println(beta);

        //Convert the list to stream and use .map to multiply by 2 each integer
        //Use the terminal operation .collector to transform the elements of the stream into a list

        Object multiply = beta.stream()
                .map(b -> b * 2)
                .collect(toList());

        //Print the list of integers

        System.out.println(multiply);
    }
}
